<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'phpDocumentor\\Reflection\\' => array($vendorDir . '/phpdocumentor/reflection-common/src', $vendorDir . '/phpdocumentor/reflection-docblock/src', $vendorDir . '/phpdocumentor/type-resolver/src'),
    'Zend\\EventManager\\' => array($vendorDir . '/zendframework/zend-eventmanager/src'),
    'Zend\\Code\\' => array($vendorDir . '/zendframework/zend-code/src'),
    'WhiteOctober\\PagerfantaBundle\\' => array($vendorDir . '/white-october/pagerfanta-bundle'),
    'Webmozart\\Assert\\' => array($vendorDir . '/webmozart/assert/src'),
    'Twig\\Extensions\\' => array($vendorDir . '/twig/extensions/src'),
    'Twig\\' => array($vendorDir . '/twig/twig/src'),
    'Tests\\' => array($baseDir . '/tests'),
    'Symfony\\WebpackEncoreBundle\\' => array($vendorDir . '/symfony/webpack-encore-bundle/src'),
    'Symfony\\Thanks\\' => array($vendorDir . '/symfony/thanks/src'),
    'Symfony\\Polyfill\\Util\\' => array($vendorDir . '/symfony/polyfill-util'),
    'Symfony\\Polyfill\\Php72\\' => array($vendorDir . '/symfony/polyfill-php72'),
    'Symfony\\Polyfill\\Php70\\' => array($vendorDir . '/symfony/polyfill-php70'),
    'Symfony\\Polyfill\\Php56\\' => array($vendorDir . '/symfony/polyfill-php56'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Polyfill\\Intl\\Idn\\' => array($vendorDir . '/symfony/polyfill-intl-idn'),
    'Symfony\\Polyfill\\Iconv\\' => array($vendorDir . '/symfony/polyfill-iconv'),
    'Symfony\\Polyfill\\Ctype\\' => array($vendorDir . '/symfony/polyfill-ctype'),
    'Symfony\\Polyfill\\Apcu\\' => array($vendorDir . '/symfony/polyfill-apcu'),
    'Symfony\\Contracts\\Service\\' => array($vendorDir . '/symfony/service-contracts'),
    'Symfony\\Component\\' => array($vendorDir . '/symfony/symfony/src/Symfony/Component'),
    'Symfony\\Cmf\\Component\\Routing\\' => array($vendorDir . '/symfony-cmf/routing/src'),
    'Symfony\\Bundle\\SwiftmailerBundle\\' => array($vendorDir . '/symfony/swiftmailer-bundle'),
    'Symfony\\Bundle\\MonologBundle\\' => array($vendorDir . '/symfony/monolog-bundle'),
    'Symfony\\Bundle\\AsseticBundle\\' => array($vendorDir . '/symfony/assetic-bundle'),
    'Symfony\\Bundle\\' => array($vendorDir . '/symfony/symfony/src/Symfony/Bundle'),
    'Symfony\\Bridge\\Twig\\' => array($vendorDir . '/symfony/symfony/src/Symfony/Bridge/Twig'),
    'Symfony\\Bridge\\ProxyManager\\' => array($vendorDir . '/symfony/symfony/src/Symfony/Bridge/ProxyManager'),
    'Symfony\\Bridge\\PhpUnit\\' => array($vendorDir . '/symfony/phpunit-bridge'),
    'Symfony\\Bridge\\Monolog\\' => array($vendorDir . '/symfony/symfony/src/Symfony/Bridge/Monolog'),
    'Symfony\\Bridge\\Doctrine\\' => array($vendorDir . '/symfony/symfony/src/Symfony/Bridge/Doctrine'),
    'Sensio\\Bundle\\GeneratorBundle\\' => array($vendorDir . '/sensio/generator-bundle'),
    'Sensio\\Bundle\\FrameworkExtraBundle\\' => array($vendorDir . '/sensio/framework-extra-bundle'),
    'Sensio\\Bundle\\DistributionBundle\\' => array($vendorDir . '/sensio/distribution-bundle'),
    'SensioLabs\\Security\\' => array($vendorDir . '/sensiolabs/security-checker/SensioLabs/Security'),
    'ScssPhp\\ScssPhp\\' => array($vendorDir . '/scssphp/scssphp/src'),
    'QueryTranslator\\' => array($vendorDir . '/netgen/query-translator/lib'),
    'Psr\\SimpleCache\\' => array($vendorDir . '/psr/simple-cache/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Link\\' => array($vendorDir . '/psr/link/src'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-factory/src', $vendorDir . '/psr/http-message/src'),
    'Psr\\Http\\Client\\' => array($vendorDir . '/psr/http-client/src'),
    'Psr\\Container\\' => array($vendorDir . '/psr/container/src'),
    'Psr\\Cache\\' => array($vendorDir . '/psr/cache/src'),
    'Prophecy\\' => array($vendorDir . '/phpspec/prophecy/src/Prophecy'),
    'PhpParser\\' => array($vendorDir . '/nikic/php-parser/lib/PhpParser'),
    'Peast\\test\\' => array($vendorDir . '/mck89/peast/test/Peast'),
    'Peast\\' => array($vendorDir . '/mck89/peast/lib/Peast'),
    'Pagerfanta\\' => array($vendorDir . '/pagerfanta/pagerfanta/src/Pagerfanta'),
    'PackageVersions\\' => array($vendorDir . '/ocramius/package-versions/src/PackageVersions'),
    'Overblog\\GraphiQLBundle\\' => array($vendorDir . '/overblog/graphiql-bundle'),
    'Overblog\\GraphQLGenerator\\' => array($vendorDir . '/overblog/graphql-bundle/lib/generator/src'),
    'Overblog\\GraphQLBundle\\' => array($vendorDir . '/overblog/graphql-bundle/src'),
    'Oneup\\FlysystemBundle\\' => array($vendorDir . '/oneup/flysystem-bundle'),
    'OndraM\\CiDetector\\' => array($vendorDir . '/ondram/ci-detector/src'),
    'Nyholm\\Psr7\\' => array($vendorDir . '/nyholm/psr7/src'),
    'Nelmio\\CorsBundle\\' => array($vendorDir . '/nelmio/cors-bundle'),
    'Monolog\\' => array($vendorDir . '/monolog/monolog/src/Monolog'),
    'Liuggio\\Fastest\\' => array($vendorDir . '/liuggio/fastest/src', $vendorDir . '/liuggio/fastest/adapters'),
    'Liip\\ImagineBundle\\' => array($vendorDir . '/liip/imagine-bundle'),
    'League\\Flysystem\\' => array($vendorDir . '/league/flysystem/src'),
    'Knp\\Menu\\' => array($vendorDir . '/knplabs/knp-menu/src/Knp/Menu'),
    'Knp\\Bundle\\MenuBundle\\' => array($vendorDir . '/knplabs/knp-menu-bundle/src'),
    'Interop\\Container\\' => array($vendorDir . '/container-interop/container-interop/src/Interop/Container'),
    'Incenteev\\ParameterHandler\\' => array($vendorDir . '/incenteev/composer-parameter-handler'),
    'Imagine\\' => array($vendorDir . '/imagine/imagine/src'),
    'Http\\Promise\\' => array($vendorDir . '/php-http/promise/src'),
    'Http\\Message\\' => array($vendorDir . '/php-http/message-factory/src'),
    'Http\\Client\\' => array($vendorDir . '/php-http/httplug/src'),
    'Hautelook\\TemplatedUriRouter\\' => array($vendorDir . '/hautelook/templated-uri-router'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'GraphQL\\' => array($vendorDir . '/webonyx/graphql-php/src'),
    'Goutte\\' => array($vendorDir . '/fabpot/goutte/Goutte'),
    'Fig\\Link\\' => array($vendorDir . '/fig/link-util/src'),
    'Faker\\' => array($vendorDir . '/fzaninotto/faker/src/Faker'),
    'FOS\\JsRoutingBundle\\' => array($vendorDir . '/friendsofsymfony/jsrouting-bundle'),
    'FOS\\HttpCache\\Tests\\' => array($vendorDir . '/friendsofsymfony/http-cache/tests'),
    'FOS\\HttpCache\\' => array($vendorDir . '/friendsofsymfony/http-cache/src'),
    'FOS\\HttpCacheBundle\\' => array($vendorDir . '/friendsofsymfony/http-cache-bundle'),
    'EzSystems\\SymfonyTools\\' => array($vendorDir . '/ezsystems/symfony-tools/src/lib'),
    'EzSystems\\SymfonyToolsBundle\\' => array($vendorDir . '/ezsystems/symfony-tools/src/bundle'),
    'EzSystems\\RepositoryForms\\Features\\' => array($vendorDir . '/ezsystems/repository-forms/features'),
    'EzSystems\\RepositoryForms\\' => array($vendorDir . '/ezsystems/repository-forms/lib'),
    'EzSystems\\RepositoryFormsBundle\\' => array($vendorDir . '/ezsystems/repository-forms/bundle'),
    'EzSystems\\PlatformInstallerBundle\\' => array($vendorDir . '/ezsystems/ezpublish-kernel/eZ/Bundle/PlatformInstallerBundle/src'),
    'EzSystems\\PlatformHttpCacheBundle\\Tests\\' => array($vendorDir . '/ezsystems/ezplatform-http-cache/tests'),
    'EzSystems\\PlatformHttpCacheBundle\\' => array($vendorDir . '/ezsystems/ezplatform-http-cache/src'),
    'EzSystems\\PlatformBehatBundle\\' => array($vendorDir . '/ezsystems/ezpublish-kernel/eZ/Bundle/PlatformBehatBundle'),
    'EzSystems\\EzSupportToolsBundle\\' => array($vendorDir . '/ezsystems/ez-support-tools'),
    'EzSystems\\EzPlatformUser\\' => array($vendorDir . '/ezsystems/ezplatform-user/src/lib'),
    'EzSystems\\EzPlatformUserBundle\\' => array($vendorDir . '/ezsystems/ezplatform-user/src/bundle'),
    'EzSystems\\EzPlatformStandardDesignBundle\\' => array($vendorDir . '/ezsystems/ezplatform-standard-design/src/bundle'),
    'EzSystems\\EzPlatformSolrSearchEngine\\Tests\\SetupFactory\\' => array($vendorDir . '/ezsystems/ezplatform-solr-search-engine/tests/lib/SetupFactory'),
    'EzSystems\\EzPlatformSolrSearchEngine\\' => array($vendorDir . '/ezsystems/ezplatform-solr-search-engine/lib'),
    'EzSystems\\EzPlatformSolrSearchEngineBundle\\' => array($vendorDir . '/ezsystems/ezplatform-solr-search-engine/bundle'),
    'EzSystems\\EzPlatformRichText\\' => array($vendorDir . '/ezsystems/ezplatform-richtext/src/lib'),
    'EzSystems\\EzPlatformRichTextBundle\\' => array($vendorDir . '/ezsystems/ezplatform-richtext/src/bundle'),
    'EzSystems\\EzPlatformMatrixFieldtype\\' => array($vendorDir . '/ezsystems/ezplatform-matrix-fieldtype/src/lib'),
    'EzSystems\\EzPlatformMatrixFieldtypeBundle\\' => array($vendorDir . '/ezsystems/ezplatform-matrix-fieldtype/src/bundle'),
    'EzSystems\\EzPlatformGraphQL\\' => array($vendorDir . '/ezsystems/ezplatform-graphql/src'),
    'EzSystems\\EzPlatformEncoreBundle\\' => array($vendorDir . '/ezsystems/ezplatform-core/src/EzPlatformEncoreBundle/bundle'),
    'EzSystems\\EzPlatformDesignEngine\\' => array($vendorDir . '/ezsystems/ezplatform-design-engine/lib'),
    'EzSystems\\EzPlatformDesignEngineBundle\\' => array($vendorDir . '/ezsystems/ezplatform-design-engine/bundle'),
    'EzSystems\\EzPlatformCronBundle\\' => array($vendorDir . '/ezsystems/ezplatform-cron/src/bundle'),
    'EzSystems\\EzPlatformCoreBundle\\' => array($vendorDir . '/ezsystems/ezplatform-core/src/EzPlatformCoreBundle/bundle'),
    'EzSystems\\EzPlatformAdminUi\\' => array($vendorDir . '/ezsystems/ezplatform-admin-ui/src/lib'),
    'EzSystems\\EzPlatformAdminUiModulesBundle\\' => array($vendorDir . '/ezsystems/ezplatform-admin-ui-modules'),
    'EzSystems\\EzPlatformAdminUiBundle\\' => array($vendorDir . '/ezsystems/ezplatform-admin-ui/src/bundle'),
    'EzSystems\\EzPlatformAdminUiAssetsBundle\\' => array($vendorDir . '/ezsystems/ezplatform-admin-ui-assets'),
    'EzSystems\\DoctrineSchema\\' => array($vendorDir . '/ezsystems/doctrine-dbal-schema/src/lib'),
    'EzSystems\\DoctrineSchemaBundle\\' => array($vendorDir . '/ezsystems/doctrine-dbal-schema/src/bundle'),
    'Egulias\\EmailValidator\\' => array($vendorDir . '/egulias/email-validator/EmailValidator'),
    'Doctrine\\ORM\\' => array($vendorDir . '/doctrine/orm/lib/Doctrine/ORM'),
    'Doctrine\\Instantiator\\' => array($vendorDir . '/doctrine/instantiator/src/Doctrine/Instantiator'),
    'Doctrine\\DBAL\\' => array($vendorDir . '/doctrine/dbal/lib/Doctrine/DBAL'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib/Doctrine/Common/Lexer'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib/Doctrine/Common/Inflector'),
    'Doctrine\\Common\\Collections\\' => array($vendorDir . '/doctrine/collections/lib/Doctrine/Common/Collections'),
    'Doctrine\\Common\\Cache\\' => array($vendorDir . '/doctrine/cache/lib/Doctrine/Common/Cache'),
    'Doctrine\\Common\\Annotations\\' => array($vendorDir . '/doctrine/annotations/lib/Doctrine/Common/Annotations'),
    'Doctrine\\Common\\' => array($vendorDir . '/doctrine/common/lib/Doctrine/Common', $vendorDir . '/doctrine/event-manager/lib/Doctrine/Common', $vendorDir . '/doctrine/persistence/lib/Doctrine/Common', $vendorDir . '/doctrine/reflection/lib/Doctrine/Common'),
    'Doctrine\\Bundle\\DoctrineCacheBundle\\' => array($vendorDir . '/doctrine/doctrine-cache-bundle'),
    'Doctrine\\Bundle\\DoctrineBundle\\' => array($vendorDir . '/doctrine/doctrine-bundle'),
    'DeepCopy\\' => array($vendorDir . '/myclabs/deep-copy/src/DeepCopy'),
    'Cron\\' => array($vendorDir . '/cron/cron/src'),
    'Composer\\CaBundle\\' => array($vendorDir . '/composer/ca-bundle/src'),
    'Buzz\\' => array($vendorDir . '/kriswallsmith/buzz/lib'),
    'Bex\\Behat\\ScreenshotExtension\\Driver\\' => array($vendorDir . '/ezsystems/behat-screenshot-image-driver-cloudinary/src'),
    'Behat\\Mink\\Driver\\' => array($vendorDir . '/behat/mink-browserkit-driver/src', $vendorDir . '/behat/mink-goutte-driver/src', $vendorDir . '/behat/mink-selenium2-driver/src'),
    'Behat\\Mink\\' => array($vendorDir . '/behat/mink/src'),
    'Bazinga\\Bundle\\JsTranslationBundle\\' => array($vendorDir . '/willdurand/js-translation-bundle'),
    'AppBundle\\' => array($baseDir . '/src/AppBundle'),
);
